# diygwUI

#### 介绍
DIY官网可视化工具做好的可视化拖拽开发工具无须编程、零代码基础、所见即所得设计工具支持轻松在线可视化导出微信小程序、支付宝小程序、头条小程序、H5、WebApp、UNIAPP等源码 支持组件库,高颜值,卡片,列表,轮播图,导航栏,按钮,标签,表单,单选,复选,下拉选择,多层选择,级联选择,开关,时间轴,模态框,步骤条,头像,进度条等
感谢colorui、uniapp、uView UI等第三方组件库的支持

引入多平台快速开发的UI框架uView UI，全面的组件和便捷的工具会让您信手拈来，如鱼得水。众多组件覆盖开发过程的各个需求，组件功能丰富，多端兼容。让您快速集成，开箱即用

丰富的按钮点击事件供选择

#### 文件说明
##### uniapp -- 对应Uniapp多端生成源码
##### weixin -- 生成微信小程序原生源码
##### alipay -- 生成支付宝小程序原生源码
##### qq -- 生成QQ小程序原生源码
##### baidu -- 生成百度小程序原生源码
##### bytedance -- 生成字节跳转小程序原生源码
##### finclip -- 生成FinClip小程序原生源码
##### dingtalk -- 生成钉钉小程序原生源码
##### html -- 生成html vue静态页面源码

更多设计前往https://www.diygw.com 设计

#### API调用截图
[![](https://agent.diygw.com/api1.png)](https://agent.diygw.com/api1.png)
[![](https://agent.diygw.com/api2.png)](https://agent.diygw.com/api2.png)
[![](https://agent.diygw.com/api3.png)](https://agent.diygw.com/api3.png)
[![](https://agent.diygw.com/api4.png)](https://agent.diygw.com/api4.png)
[![](https://agent.diygw.com/api5.png)](https://agent.diygw.com/api5.png)
[![](https://agent.diygw.com/api6.png)](https://agent.diygw.com/api6.png)
[![](https://agent.diygw.com/api7.png)](https://agent.diygw.com/api7.png)
[![](https://agent.diygw.com/api8.png)](https://agent.diygw.com/api8.png)

#### 项目截图
[![](https://agent.diygw.com/new1.png)](https://agent.diygw.com/new1.png)
[![](https://agent.diygw.com/new2.png)](https://agent.diygw.com/new2.png)
[![](https://agent.diygw.com/new4.png)](https://agent.diygw.com/new4.png)
[![](https://agent.diygw.com/new3.png)](https://agent.diygw.com/new3.png)
[![](https://agent.diygw.com/exporttype.png)](https://agent.diygw.com/exporttype.png)
[![](https://agent.diygw.com/ok4.png)](https://agent.diygw.com/ok4.png)
[![](https://agent.diygw.com/ok5.png)](https://agent.diygw.com/ok5.png)
[![](https://agent.diygw.com/ok6.png)](https://agent.diygw.com/ok6.png)
[![](https://agent.diygw.com/ok7.png)](https://agent.diygw.com/ok7.png)
[![](https://agent.diygw.com/1.png)](https://agent.diygw.com/1.png)
[![](https://agent.diygw.com/1.png)](https://agent.diygw.com/1.png)
[![](https://agent.diygw.com/2.png)](https://agent.diygw.com/2.png)
[![](https://agent.diygw.com/3.png)](https://agent.diygw.com/3.png)
[![](https://agent.diygw.com/4.png)](https://agent.diygw.com/4.png)
[![](https://agent.diygw.com/5.png)](https://agent.diygw.com/5.png)
[![](https://agent.diygw.com/6.png)](https://agent.diygw.com/6.png)
[![](https://agent.diygw.com/7.png)](https://agent.diygw.com/7.png)
[![](https://agent.diygw.com/8.png)](https://agent.diygw.com/8.png)
[![](https://agent.diygw.com/9.png)](https://agent.diygw.com/9.png)
[![](https://agent.diygw.com/10.png)](https://agent.diygw.com/10.png)
[![](https://agent.diygw.com/11.png)](https://agent.diygw.com/11.png)
[![](https://agent.diygw.com/12.png)](https://agent.diygw.com/12.png)
[![](https://agent.diygw.com/1.jpg)](https://agent.diygw.com/1.jpg)
[![](https://agent.diygw.com/2.jpg)](https://agent.diygw.com/2.jpg)
[![](https://agent.diygw.com/3.jpg)](https://agent.diygw.com/3.jpg)
[![](https://agent.diygw.com/4.jpg)](https://agent.diygw.com/4.jpg)
[![](https://agent.diygw.com/5.jpg)](https://agent.diygw.com/5.jpg)
[![](https://agent.diygw.com/6.jpg)](https://agent.diygw.com/6.jpg)
[![](https://agent.diygw.com/7.jpg)](https://agent.diygw.com/7.jpg)


#### 扫码体验

在线演示例子：[组件库演示](https://uniapp.diygw.com) https://uniapp.diygw.com

扫码体验

[![](https://agent.diygw.com/uniapp.png)](https://agent.diygw.com/uniapp.png)

在线演示例子：[仿应用市场例子](https://uniapp.diygw.com/app/#/)  https://uniapp.diygw.com/app/#/

扫码体验

[![](https://agent.diygw.com/uniapp-app.png)](https://agent.diygw.com/uniapp-app.png)

小程序扫码体验

[![](https://agent.diygw.com/diygwcom.jpg)](https://agent.diygw.com/diygwcom.jpg)



更多版本兼容性在持续完善中


